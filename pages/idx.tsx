// import './App.css';
import { useState } from 'react';

import CeramicClient from '@ceramicnetwork/http-client';
import ThreeIdResolver from '@ceramicnetwork/3id-did-resolver';
import { Caip10Link } from '@ceramicnetwork/stream-caip10-link'
import { EthereumAuthProvider, ThreeIdConnect } from '@3id/connect';
import { DID } from 'dids';
import { IDX } from '@ceramicstudio/idx';

const endpoint = 'https://ceramic-clay.3boxlabs.com';
var did1 = "";
export default function App() {
  const [name, setName] = useState('');
  const [loaded, setLoaded] = useState(false);

  async function connect() {
    const addresses = await window.ethereum.request({
      method: 'eth_requestAccounts',
    });
    return addresses;
  }

  // reading the profile if it is set

  async function readProfile() {
    const [address] = await connect();
    console.log('a', address);
    const ceramic = new CeramicClient(endpoint);
    const idx = new IDX({ ceramic });

    try {
      const data = await idx.get<{ name: string;}>('basicProfile', `${address}@eip155:1`);
      console.log('data: ', data);
      if (data?.name) setName(data?.name);
    } catch (error) {
      console.log('error: ', error);
      setLoaded(true);
    }
  }

  // updating the current profile

  async function updateProfile() {
    const [address] = await connect();
    const ceramic = new CeramicClient(endpoint);
    const threeIdConnect = new ThreeIdConnect();
    const provider = new EthereumAuthProvider(window.ethereum, address);

    await threeIdConnect.connect(provider);

    console.log("threeIdConnect connected");
    

    const did = new DID({
      provider: threeIdConnect.getDidProvider(),
      resolver: {
        ...ThreeIdResolver.getResolver(ceramic),
      },
    });

    await did.authenticate();
    ceramic.setDID(did);
    console.log("DID:",did)
    console.log("Authenticated");
    

    const idx = new IDX({ ceramic });

    await idx.set('basicProfile', {
      name,
    });

    console.log('Profile updated!');
  }

  async function getLinkedDID() {
    const [address] = await connect()
    const ceramic = new CeramicClient(endpoint)
  const link = await Caip10Link.fromAccount(
    ceramic,
    `${address}@eip155:1`,
  )
  console.log(link.id.toUrl())
  console.log("Eth address:",[address])
  console.log(link.did)
  did1 = link.did
  return link.did
}
  async function linkCurrentAddress() {

    const addresses = await window.ethereum.request({
      method: 'eth_requestAccounts',
    })
    const ceramic = new CeramicClient(endpoint);
    const authProvider = new EthereumAuthProvider(window.ethereum, addresses[0])

    const accountId = await authProvider.accountId()

    const accountLink = await Caip10Link.fromAccount(
      ceramic,
      accountId.toString(),
    )

    if did1 != ""
    {
    await accountLink.setDid(
      did1,
      authProvider,
  )
    console.log("Success!");
    console.log(accountLink)
    }
    else
    {
      console("No DID being used as of now")
    }
    
}

  return (
    <div className="App">
      <input placeholder='Name' onChange={(e) => setName(e.target.value)} />
      <button onClick={updateProfile}>Set Profile</button>
      <button onClick={readProfile}>Read Profile</button>
      <button onClick={getLinkedDID}>Get Link</button>
      <button onClick={linkCurrentAddress}>Link new account</button>

      {name && <h3>{name}</h3>}
      {!name && loaded && <h4>No profile, please create one...</h4>}
    </div>
  );
}
