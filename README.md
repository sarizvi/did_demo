# Decentralized Identifier demo

This project demonstrates basic usage of ceramic network and its libraries of caip10-link and blockchain-utils-linking on Next.js

### `Prequisites`

 - Metamask should be setup on your chrome. Link: https://metamask.io/download/
 - Node should be installed. Command: `sudo apt install nodejs`
 - npm should be installed. Command: `sudo apt-get install npm`


### `Setup`
 - Create a next app using command: `npx create-next-app@latest --ts`
 - Go inside the project directory and install the packages using `npm install @3id/connect @ceramicnetwork/3id-did-resolver @ceramicnetwork/http-client @ceramicstudio/idx dids ethers @ceramicnetwork/stream-caip10-link`

### `Usage`

 - Run the app using `npm run dev`
 - Once the app starts, navigate to "localhost:3000/idx". Right click on browser window, click inspect and open console.
 - Here you can read profile for the current account and set basic name as well using Read Profile and Set Profile respectively.
 - You can also get current link between the currently connected account and DID using Get Link.
 - Once you load the link and DID then you can use the same DID to map new blockchain account to that DID. Currently, it supports only evm networks (eip155:1), working on to extend support to other blockchains as well.
 - If it is successfully mapped, you will receive "Success" message in the console.

